import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@/pages/home"),
    meta: { title: "Портал «Развиваем Липецкую область вместе!»" },
  },
  {
    path: "/houses",
    name: "houses",
    component: () => import("@/pages/houses"),
    meta: { title: "Обманутые дольщики" },
  },
  {
    path: "/houses/:id",
    name: "houses-detail",
    component: () => import("@/pages/houses-detail"),
    meta: { title: "Обманутые дольщики - детально" },
  },
  {
    path: "/agree",
    name: "agree",
    component: () => import("@/pages/agree"),
    meta: { title: "Персональные данные" },
  },
  {
    path: "/rules",
    name: "rules",
    component: () => import("@/pages/rules"),
    meta: { title: "Правила сайта" },
  },
  {
    path: "/bot_rules_agreement",
    name: "bot_rules_agreement",
    component: () => import("@/pages/bot-rules-agreement"),
    meta: {
      title: "bot_rules_agreement",
      layout: "empty-layout",
      footerLayout: "empty-layout",
    },
  },
  {
    path: "/hot/:id",
    name: "hot-detail",
    component: () => import("@/pages/hot"),
    meta: { title: "Портал «Развиваем Липецкую область вместе!»" },
  },
  // 404
  {
    path: "*",
    name: "404",
    component: () => import("@/pages/404"),
    meta: { title: "Страница Не Найдена" },
  },
];

const router = new VueRouter({
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  },
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
