import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueAwesomeSwiper from "vue-awesome-swiper";
import Vue2TouchEvents from "vue2-touch-events";

import empty from "./layouts/empty";
import defaultLayout from "./layouts/app-header";
import footerLayout from "./layouts/app-footer";

Vue.component("empty-layout", empty);
Vue.component("default-layout", defaultLayout);
Vue.component("footer-layout", footerLayout);

import "img-comparison-slider";

import "swiper/css/swiper.css";

Vue.config.productionTip = false;
Vue.config.ignoredElements = [/img-comparison-slider/];
Vue.use(Vue2TouchEvents);
new Vue({
  router,
  store,
  VueAwesomeSwiper,
  Vue2TouchEvents,
  render: (h) => h(App),
}).$mount("#app");
