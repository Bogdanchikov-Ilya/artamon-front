import axios from "axios";

export const api = axios.create({
  headers: {
    "Content-type": "application/json",
  },
  baseURL: process.env.VUE_APP_DOMEN,
  responseType: "json",
});
