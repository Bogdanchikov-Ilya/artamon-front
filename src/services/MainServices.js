import { api } from "../api/api";
import store from "../store";

class MainServices {
  async loadItems(url) {
    try {
      const result = await api.get(url);
      return result.data;
    } catch (e) {
      throw e;
    }
  }
  getItemDetail(url) {
    if (!store.getters.getDetail)
      this.loadItems(url).then((item) => store.commit("setDetail", item));
    return store.getters.getDetail;
  }
  getItems(url) {
    switch (url) {
      case "/api/menus":
        if (!store.getters.getMenus)
          this.loadItems(url).then((items) => store.commit("setMenus", items));
        return store.getters.getMenus;
      case "/api/hot_topics":
        if (!store.getters.getThemes)
          this.loadItems(url).then((items) => store.commit("setThemes", items));
        return store.getters.getThemes;
      case "/api/results":
        if (!store.getters.getResults)
          this.loadItems(url).then((items) =>
            store.commit("setResults", items)
          );
        return store.getters.getResults;
      case "/api/statistics":
        if (!store.getters.getStatistics)
          this.loadItems(url).then((items) =>
            store.commit("setStatistics", items)
          );
        return store.getters.getStatistics;
      case "/api/fragments":
        if (!store.getters.getFragments)
          this.loadItems(url).then((items) =>
            store.commit("setFragments", items)
          );
        return store.getters.getFragments;
      case "/api/resources":
        if (!store.getters.getResources)
          this.loadItems(url).then((items) =>
            store.commit("setResources", items)
          );
        return store.getters.getResources;
      case "/api/banners":
        if (!store.getters.getBanners)
          this.loadItems(url).then((items) =>
            store.commit("setBanners", items)
          );
        return store.getters.getBanners;
      case "/api/houses":
        if (!store.getters.getHouses)
          this.loadItems(url).then((items) => store.commit("setHouses", items));
        return store.getters.getHouses;
      case "/api/hot_companies":
        if (!store.getters.getCompanies)
          this.loadItems(url).then((items) =>
            store.commit("setCompanies", items)
          );
        return store.getters.getCompanies;
      case "/api/hot_docs":
        if (!store.getters.getDocs)
          this.loadItems(url).then((items) => store.commit("setDocs", items));
        return store.getters.getDocs;
    }
  }
}

export default MainServices;
