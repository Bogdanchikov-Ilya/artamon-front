import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    menus: null,
    themes: null,
    results: null,
    statistics: null,
    fragments: null,
    resources: null,
    banners: null,
    houses: null,
    companies: null,
    docs: null,

    //getDetailItem
    detail: null,
    // env URL
    url: process.env.VUE_APP_DOMEN,
  },
  mutations: {
    setMenus(state, data) {
      state.menus = data;
    },
    setThemes(state, data) {
      state.themes = data;
    },
    setResults(state, data) {
      state.results = data;
    },
    setStatistics(state, data) {
      state.statistics = data;
    },
    setFragments(state, data) {
      state.fragments = data;
    },
    setResources(state, data) {
      state.resources = data;
    },
    setBanners(state, data) {
      state.banners = data;
    },
    setHouses(state, data) {
      state.houses = data;
    },
    setCompanies(state, data) {
      state.companies = data;
    },
    setDocs(state, data) {
      state.docs = data;
    },
    setDetail(state, data) {
      state.detail = data;
    },
  },
  getters: {
    getApiUrl(state) {
      return state.url;
    },
    // components
    getMenus(state) {
      return state.menus;
    },
    getThemes(state) {
      return state.themes;
    },
    getResults(state) {
      return state.results;
    },
    getStatistics(state) {
      return state.statistics;
    },
    getFragments(state) {
      return state.fragments;
    },
    getResources(state) {
      return state.resources;
    },
    getBanners(state) {
      return state.banners;
    },
    getHouses(state) {
      return state.houses;
    },
    getCompanies(state) {
      return state.companies;
    },
    getDocs(state) {
      return state.docs;
    },
    getDetail(state) {
      return state.detail;
    },
  },
});
